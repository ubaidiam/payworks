//
//  CreditCard.swift
//  CCEvaluator
//
//  Created by Muhammad Ubaid on 9/14/18.
//  Copyright © 2018 Muhammad Ubaid. All rights reserved.
//

import Foundation

public class CreditCardValidator
{
    public func isValidCardNumber( cardNumber: String) -> Bool
    {
        var isValidCardNumber = true
        let creditCardNumber = cardNumber.replacingOccurrences(of: " ", with: "")
        if !creditCardNumber.isStringContainsOnlyNumbers() {
            isValidCardNumber = false
        }
        else if creditCardNumber.hasPrefix("0") {
            isValidCardNumber = false
        }
        else if !isDigitCountBetween(cardNumber: creditCardNumber, minCount: 12, maxCount: 19) {
            isValidCardNumber = false
        }
        else if !creditCardNumber.isValidCreditCardNumber() {
            isValidCardNumber = false
        }
        
        return isValidCardNumber
    }
    
    fileprivate func isDigitCountBetween(cardNumber: String, minCount: Int, maxCount: Int) -> Bool
    {
        let count = cardNumber.count
        return (count >= minCount && count <= maxCount)
    }
}
