//
//  String+Number.swift
//  CCEvaluator
//
//  Created by Muhammad Ubaid on 9/17/18.
//  Copyright © 2018 Muhammad Ubaid. All rights reserved.
//

import Foundation

internal extension String {
    
    func isStringContainsOnlyNumbers() -> Bool {
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
    }
}
