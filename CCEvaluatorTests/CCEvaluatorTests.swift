//
//  CCEvaluatorTests.swift
//  CCEvaluatorTests
//
//  Created by Muhammad Ubaid on 9/14/18.
//  Copyright © 2018 Muhammad Ubaid. All rights reserved.
//

import XCTest
@testable import CCEvaluator

class CCEvaluatorTests: XCTestCase {
    
    var evaluator: CreditCardValidator!
    
    override func setUp() {
        super.setUp()
        evaluator = CreditCardValidator()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testValidCreditCardNumber() {
        // This is an example of a functional test case.
        XCTAssert(evaluator.isValidCardNumber(cardNumber: "123456789"))
        XCTAssert(evaluator.isValidCardNumber(cardNumber: "0123456789"))
        XCTAssert(evaluator.isValidCardNumber(cardNumber: "4111 1111 1111 1111"))
        XCTAssert(evaluator.isValidCardNumber(cardNumber: "05500000000000004"))
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
